Hi-5 detector for Ti SimpleLink™ CC2650 SensorTag™

A school project for University of Oulu's Computer Systems course. Detects (not very reliably) three different gestures by comparing standard deviation of six different sensors found in the MPU9250 motion processing unit: X-, Y- and Z-axis gyroscopes and accelerometers.

Included in the repo are:
- Some reference measurements for the different gestures. These were used to calculate thresholds for detecting the gestures. In the measurements folder.
- plan.jpg: quick sketch of how the state machine works and what kind of screens the device will have.
- main.c, which is mostly written by me. In the src folder.
- mpu9250.c, in which I have written most of the mpu9250_get_data()-function. The function can be found at the end of the file. In the src folder.
