
#include <stdio.h>
#include <string.h>

// **************************************************************
//
// XDCTOOLS FILES
//
// *******************************

#include <xdc/std.h>
#include <xdc/runtime/System.h>

// **************************************************************
//
// BIOS HEADER FILES
//
// *******************************

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC26XX.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/mw/display/Display.h>
#include <ti/mw/display/DisplayExt.h>

// **************************************************************
//
// BOARD HEADER FILES
//
// *******************************

#include "Board.h"
#include "wireless/comm_lib.h"
#include "sensors/bmp280.h"
#include "sensors/mpu9250.h"


// **************************************************************
//
// TASKS
//
// *******************************

#define STACKSIZE 2048
Char stateTaskStack[STACKSIZE];
Char commTaskStack[STACKSIZE];

// **************************************************************
//
// STATES!!!
//
// *******************************

enum state{ idle=1, read_sensor, msg_waiting };

enum state myState = idle;

// **************************************************************
//
// MPU GLOBAL VARIABLES
//
// *******************************

static PIN_Handle hMpuPin;
static PIN_State MpuPinState;
static PIN_Config MpuPinConfig[] = {
    Board_MPU_POWER  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

// **************************************************************
//
// OTHER GLOBAL VARIABLES
//
// *******************************

float global_ax[200], global_ay[200], global_az[200], global_gx[200], global_gy[200], global_gz[200];
int index = 0;

// **************************************************************
//
// MPU9250 I2C CONFIG
//
// *******************************

static const I2CCC26XX_I2CPinCfg i2cMPUCfg = {
    .pinSDA = Board_I2C0_SDA1,
    .pinSCL = Board_I2C0_SCL1
};

// **************************************************************
//
// DISPLAY 
//
// *******************************

Display_Handle hDisplay;

// **************************************************************
//
// PAINONAPPIEN KONFIGURAATIO JA MUUTTUJAT
//
// *******************************

static PIN_Handle buttonHandle;
static PIN_State buttonState;

static PIN_Handle ledHandle;
static PIN_State ledState;

PIN_Config buttonConfig[] = {
   Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_BOTHEDGES, // Hox! TAI-operaatio
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

PIN_Config ledConfig[] = {
   Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX, 
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

// **************************************************************
//
// VIRTANAPPI!!!
//
// *******************************

static PIN_Handle hButtonShut;
static PIN_State bStateShut;
PIN_Config buttonShut[] = {
   Board_BUTTON1 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
   PIN_TERMINATE
};
PIN_Config buttonWake[] = {
   Board_BUTTON1 | PIN_INPUT_EN | PIN_PULLUP | PINCC26XX_WAKEUP_NEGEDGE,
   PIN_TERMINATE
};

// **************************************************************
//
// SENSORINLUKUTASKI!!!
//
// *******************************

Void read_sensor_values() {

    // **************************************************************
    //
    // USE TWO DIFFERENT I2C INTERFACES
    //
    // *******************************
	//I2C_Handle i2c; // INTERFACE FOR OTHER SENSORS
	//I2C_Params i2cParams;
	I2C_Handle i2cMPU; // INTERFACE FOR MPU9250 SENSOR
	I2C_Params i2cMPUParams;

	float ax, ay, az, gx, gy, gz;
	// double pres,temp;
	// char str[80];
	char test[256];

    //I2C_Params_init(&i2cParams);
    //i2cParams.bitRate = I2C_400kHz;

    I2C_Params_init(&i2cMPUParams);
    i2cMPUParams.bitRate = I2C_400kHz;
    i2cMPUParams.custom = (uintptr_t)&i2cMPUCfg;

    // **************************************************************
    //
    // MPU OPEN I2C
    //
    // *******************************
    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
    if (i2cMPU == NULL) {
        System_abort("Error Initializing I2CMPU\n");
    }

    // **************************************************************
    //
    // MPU POWER ON
    //
    // *******************************
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_ON);

    // WAIT 100MS FOR THE SENSOR TO POWER UP
	Task_sleep(100000 / Clock_tickPeriod);
    System_printf("MPU9250: Power ON\n");
    System_flush();

    // **************************************************************
    //
    // MPU9250 SETUP AND CALIBRATION
    //
    // *******************************
	System_printf("MPU9250: Setup and calibration...\n");
	System_flush();

	mpu9250_setup(&i2cMPU);

	System_printf("MPU9250: Setup and calibration OK\n");
	System_flush();

    // **************************************************************
    //
    // MPU CLOSE I2C
    //
    // *******************************
    I2C_close(i2cMPU);

    // **************************************************************
    //
    // BMP280 OPEN I2C
    //
    // *******************************
    //i2c = I2C_open(Board_I2C, &i2cParams);
    //if (i2c == NULL) {
    //    System_abort("Error Initializing I2C\n");
    //}

    // BMP280 SETUP
    //bmp280_setup(&i2c);

    // **************************************************************
    //
    // BMP280 CLOSE I2C
    //
    // *******************************
    I2C_close(i2c);
    
    // Display //
    //Display_Params displayParams;
	//displayParams.lineClearMode = DISPLAY_CLEAR_BOTH;
    //Display_Params_init(&displayParams);

    //hDisplay = Display_open(Display_Type_LCD, &displayParams);
    //if (hDisplay == NULL) {
    //    System_abort("Error initializing Display\n");
    //}
    // LOOP FOR FIVE SECONDS
    double sensor_read_start = 0;
    int index = 0;
    sensor_read_start = Clock_getTicks();

	while (index<200) {
	    // **************************************************************
	    //
	    // BMP280 OPEN I2C
	    //
	    // *******************************
	    //i2c = I2C_open(Board_I2C, &i2cParams);
	    //if (i2c == NULL) {
	    //    System_abort("Error Initializing I2C\n");
	    //}

	    // **************************************************************
	    //
	    // BMP280 ASK DATA
	    //
	    // *******************************
	    //bmp280_get_data(&i2c, &pres, &temp);

        // DO SOMETHING WITH THE BMP280 DATA

	    // **************************************************************
	    //
	    // BMP280 CLOSE I2C
	    //
	    // *******************************
	    //I2C_close(i2c);

	    // **************************************************************
	    //
	    // MPU OPEN I2C
	    //
	    // *******************************
	    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
	    if (i2cMPU == NULL) {
	        System_abort("Error Initializing I2CMPU\n");
	    }

	    // **************************************************************
	    //
	    // MPU ASK DATA
		//
        //    Accelerometer values: ax,ay,az
	 	//    Gyroscope values: gx,gy,gz
		//
	    // *******************************
		mpu9250_get_data(&i2cMPU, &ax, &ay, &az, &gx, &gy, &gz);
		global_ax[i] = ax;
		global_ay[i] = ay;
		global_az[i] = az;
		global_gx[i] = gx;
		global_gy[i] = gy;
		global_gz[i] = gz;
        
        // DO SOMETHING WITH THE DATA
        //memset(test, 0, 256);
        //sprintf(test, "%f | %f | %f | %f | %f | %f \n", ax, ay, az, gx, gy, gz);
        //System_printf(test);
        //System_flush();

	    // **************************************************************
	    //
	    // MPU CLOSE I2C
	    //
	    // *******************************
	    I2C_close(i2cMPU);
	    index++;
	    Task_sleep(25000 / Clock_tickPeriod);
	}

	// MPU9250 POWER OFF 
	// Because of loop forever, code never goes here
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_OFF);
    myState = idle;
}

/* WANHA SENSORINLUKUTASKI!!!
Void labTaskFxn(UArg arg0, UArg arg1) {

    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Handle i2cMPU; // INTERFACE FOR MPU9250 SENSOR
	I2C_Params i2cMPUParams;
	
    char airpressure[16];
    char temperature[16];
    double pres, temp;
    
    // Create I2C for sensors //
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    i2c = I2C_open(Board_I2C0, &i2cParams);
    if (i2c == NULL) {
        System_abort("Error Initializing I2C\n");
    }
    else {
        System_printf("I2C Initialized!\n");
    }

    // JTKJ: Setup the BMP280 sensor here, before its use
    // JTKJ: Sensorin alustus t�ss� kirjastofunktiolla
    bmp280_setup(&i2c);

    // Display //
    Display_Params displayParams;
	displayParams.lineClearMode = DISPLAY_CLEAR_BOTH;
    Display_Params_init(&displayParams);

    hDisplay = Display_open(Display_Type_LCD, &displayParams);
    if (hDisplay == NULL) {
        System_abort("Error initializing Display\n");
    }


    while (1) {

        // DO SOMETHING HERE..
        bmp280_get_data(&i2c, &pres, &temp);
        sprintf(airpressure, "%lf", pres/100);
        sprintf(temperature, "%lf", (temp-32.0)*(5.0/9.0));
        Display_print0(hDisplay, 1, 1, "Air pressure:");
        Display_print0(hDisplay, 2, 1, airpressure);
        Display_print0(hDisplay, 3, 1, "Temperature:");
        Display_print0(hDisplay, 4, 1, temperature);
    	// Once per second
    	Task_sleep(6000000 / Clock_tickPeriod);
    }
}
*/

// **************************************************************
//
// KOMMUNIKAATIO!!!
//
// *******************************

Void commTaskFxn(UArg arg0, UArg arg1) {
    
   char payload[16]; // viestipuskuri
   uint16_t senderAddr;
   
    // Radio to receive mode
	int32_t result = StartReceive6LoWPAN();
	if(result != true) {
		System_abort("Wireless receive mode failed");
	}

    while (1) {

        // If true, we have a message
        if (GetRXFlag() && myState == idle) {
            myState = msg_waiting;
        }

    // Absolutely NO Task_sleep in this task!!
    }
}

uint16_t handle_message(Void) {
    
   char payload[16]; // viestipuskuri
   uint16_t senderAddr;
   
    // Radio to receive mode
	//int32_t result = StartReceive6LoWPAN();
	//if(result != true) {
	//	System_abort("Wireless receive mode failed");
	//}


    memset(payload,0,16);
    // Luetaan viesti puskuriin payload
    Receive6LoWPAN(&senderAddr, payload, 16);
    // Tulostetaan vastaanotettu viesti konsoli-ikkunaan
    strcat(payload, "\n");
    System_printf(payload);
    System_flush();
    
    return senderAddr;
}

Void send_reply(senderAddr) {
    
   char payload[] = "NICE!"; // viestipuskuri
   
    Send6LoWPAN (senderAddr, payload, strlen(payload));
}

// **************************************************************
//
// PAINONAPIN KÄSITTELIJÄFUNKTIO
//
// *******************************

void buttonFxn(PIN_Handle handle, PIN_Id pinId) {
    
    if (myState == idle) {
        myState = read_sensor;
        PIN_setOutputValue( ledHandle, Board_LED1, !PIN_getOutputValue( Board_LED1 ) );
    }
    
    // char payload[5] = "Jou.";
    
    // Send6LoWPAN (IEEE80154_SERVER_ADDR, payload, strlen(payload));
}

// **************************************************************
//
// VIRTANAPIN KÄSITTELIJÄFUNKTIO
//
// *******************************

Void buttonShutFxn(PIN_Handle handle, PIN_Id pinId) {

   // Näyttö pois päältä
   Display_clear(hDisplay);
   Display_close(hDisplay);
   Task_sleep(100000 / Clock_tickPeriod);

   // Itse taikamenot
   PIN_close(hButtonShut);
   PINCC26XX_setWakeup(buttonWake);
   Power_shutdown(NULL,0);
}

// **************************************************************
//
// STATETASK
//
// *******************************

Void stateTask(UArg arg0, UArg arg1) {

	while (1) {
	
        // Tilakoneen toteutus
        switch (myState) {

            case read_sensor:
                // Suoritetaan tilan vaatima operaatio
                read_sensor_values();
                update_screen();
                // Asetetaan tila takaisin IDLE
                myState = idle;				
                break;

            case msg_waiting:
                // Suoritetaan tilan vaatima operaatio
                senderAddr = handle_message();
                send_reply(senderAddr);			
                // Asetetaan tila takaisin IDLE
                myState = idle;				
                break;			
        }
    }
}
// **************************************************************
//
// MAIN
//
// *******************************

Int main(void) {

    // Task variables
	Task_Handle stateTask;
	Task_Params stateTaskParams;
	Task_Handle commTask;
	Task_Params commTaskParams;
	

    // Initialize board
    Board_initGeneral();
    Board_initI2C();

    // **************************************************************
    //
    // OPEN MPU POWER PIN
    //
    // *******************************
    
    hMpuPin = PIN_open(&MpuPinState, MpuPinConfig);
    if (hMpuPin == NULL) {
    	System_abort("Pin open failed!");
    }
    
	// JTKJ: Open and configure the button and led pins here
    // JTKJ: Painonappi- ja ledipinnit k�ytt��n t�ss�
    buttonHandle = PIN_open(&buttonState, buttonConfig);
    if(!buttonHandle) {
       System_abort("Error initializing button pins\n");
    }
    ledHandle = PIN_open(&ledState, ledConfig);
    
    if(!ledHandle) {
       System_abort("Error initializing LED pins\n");
    }
    
    // VIRTANAPPI!!!
    hButtonShut = PIN_open(&bStateShut, buttonShut);
    if( !hButtonShut ) {
      System_abort("Error initializing button shut pins\n");
    }
    if (PIN_registerIntCb(hButtonShut, &buttonShutFxn) != 0) {
       System_abort("Error registering button callback function");
    }


	// JTKJ: Register the interrupt handler for the button
    // JTKJ: Rekister�i painonapille keskeytyksen k�sittelij�funktio
   if (PIN_registerIntCb(buttonHandle, &buttonFxn) != 0) {
      System_abort("Error registering button callback function");
   }
   
    /* Task */
    Task_Params_init(&stateTaskParams);
    stateTaskParams.stackSize = STACKSIZE;
    stateTaskParams.stack = &stateTaskStack;
    stateTaskParams.priority=2;

    stateTask = Task_create((Task_FuncPtr)stateTask, &stateTaskParams, NULL);
    if (stateTask == NULL) {
    	System_abort("Task create failed!");
    }

    /* Communication Task */
    Init6LoWPAN(); // This function call before use!

    Task_Params_init(&commTaskParams);
    commTaskParams.stackSize = STACKSIZE;
    commTaskParams.stack = &commTaskStack;
    commTaskParams.priority=1;

    commTask = Task_create(commTaskFxn, &commTaskParams, NULL);
    if (commTask == NULL) {
    	System_abort("Task create failed!");
    }

    System_printf("Hello world!\n");
    System_flush();
    
    /* Start BIOS */
    BIOS_start();

    return (0);
}


