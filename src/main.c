// **************************************************************
//
// JTKJ HARJOITUSTYÖ 2019 A.K.A HIGH-FIVE/BYEBYE/MARSMARS DETECTOR
// 
// TEKIJÄ: JUHO HOLMI
//
// **************************************************************

#include <stdio.h>
#include <string.h>
#include <math.h>

// **************************************************************
//
// XDCTOOLS FILES
//
// *******************************

#include <xdc/std.h>
#include <xdc/runtime/System.h>

// **************************************************************
//
// BIOS HEADER FILES
//
// *******************************

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC26XX.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/mw/display/Display.h>
#include <ti/mw/display/DisplayExt.h>

// **************************************************************
//
// BOARD HEADER FILES
//
// *******************************

#include "Board.h"
#include "wireless/comm_lib.h"
#include "sensors/bmp280.h"
#include "sensors/mpu9250.h"


// **************************************************************
//
// TASKS
//
// *******************************

#define STACKSIZE 2048
Char stateTaskStack[STACKSIZE];
Char commTaskStack[STACKSIZE];
Char screenTaskStack[STACKSIZE];
Char sensorTaskStack[STACKSIZE];


// **************************************************************
//
// STATES!!!
//
// *******************************

enum state{ idle=1, read_sensor, msg_waiting, menu_detect, menu_back_to_idle, 
        menu_shutdown };
enum state myState = idle;


// **************************************************************
//
// MPU GLOBAL VARIABLES
//
// *******************************

static PIN_Handle hMpuPin;
static PIN_State MpuPinState;
static PIN_Config MpuPinConfig[] = {
    Board_MPU_POWER  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

// **************************************************************
//
// OTHER GLOBAL VARIABLES
//
// *******************************

float global_ax[200], global_ay[200], global_az[200], global_gx[200], 
    global_gy[200], global_gz[200];
int index = 0;

// **************************************************************
//
// MPU9250 I2C CONFIG
//
// *******************************

static const I2CCC26XX_I2CPinCfg i2cMPUCfg = {
    .pinSDA = Board_I2C0_SDA1,
    .pinSCL = Board_I2C0_SCL1
};

// **************************************************************
//
// DISPLAY 
//
// *******************************

Display_Handle hDisplay;

// **************************************************************
//
// YLEMMÄN NAPIN KONFIGURAATIO JA MUUTTUJAT
//
// *******************************

static PIN_Handle upperButtonHandle;
static PIN_State upperButtonState;

PIN_Config upperButtonConfig[] = {
   Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE, // Hox! TAI-operaatio
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

// **************************************************************
//
// LEDIEN KONFIGURAATIO JA MUUTTUJAT
//
// *******************************

static PIN_Handle ledHandle;
static PIN_State ledState;

PIN_Config ledConfig[] = {
   Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX, 
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

// **************************************************************
//
// ALEMPI NAPPI (VIRTANAPPI)!!!
//
// *******************************

static PIN_Handle lowerButtonHandle;
static PIN_State lowerButtonState;

PIN_Config lowerButtonConfig[] = {
   Board_BUTTON1 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
   PIN_TERMINATE
};
PIN_Config buttonWake[] = {
   Board_BUTTON1 | PIN_INPUT_EN | PIN_PULLUP | PINCC26XX_WAKEUP_NEGEDGE,
   PIN_TERMINATE
};

// **************************************************************
//
// KESKIHAJONNAN LASKU!!!
// Lainattu osoitteesta: 
// https://www.programiz.com/c-programming/examples/standard-deviation
//
// *******************************

double calculate_standard_deviation(float data[])
{
    double sum = 0.0, mean, standardDeviation = 0.0;
    int i;
    for(i=0; i<200; i++)
    {
        sum += data[i];
    }
    mean = sum/200;
    for(i=0; i<200; ++i)
        standardDeviation += pow(data[i] - mean, 2);
    return sqrt(standardDeviation/200);
}

// **************************************************************
//
// ELEENTUNNISTUSFUNKTIO!!!
//
// *******************************

Void detect_gestures() {
    char payload[16];
    double sd_acc_x, sd_acc_y, sd_acc_z, sd_gyro_x, sd_gyro_y, sd_gyro_z;
    
    sd_acc_x = calculate_standard_deviation(global_ax);
    sd_acc_y = calculate_standard_deviation(global_ay);
    sd_acc_z = calculate_standard_deviation(global_az);
    sd_gyro_x = calculate_standard_deviation(global_gx);
    sd_gyro_y = calculate_standard_deviation(global_gy);
    sd_gyro_z = calculate_standard_deviation(global_gz);
    
    sprintf(test, "SD: Acc_x: %f, Acc_y: %f, Acc_z: %f, Gyro_x: %f, Gyro_y: %f,Gyro_z: %f\n", sd_acc_x, sd_acc_y, 
            sd_acc_z, sd_gyro_x, sd_gyro_y, sd_gyro_z);
    System_printf(test);
    System_flush();
    
    if (sd_acc_x > 0.15 && sd_acc_x < 0.7 && sd_gyro_z > 18 && 
    sd_gyro_z < 65 && sd_acc_z > 0.4 && sd_acc_z < 0.9) {
        Display_clear(hDisplay);
        Display_print0(hDisplay, 1, 1, "High five");
        Display_print0(hDisplay, 2, 1, "detected!");
        strcpy(payload, "Hi5!"); // viestipuskuri
        Send6LoWPAN (0xFFFF, payload, strlen(payload));
        StartReceive6LoWPAN();
        myState = msg_waiting;
    } else if (sd_acc_x > 0.7 && sd_acc_x < 2.2 && sd_gyro_z > 110 && 
    sd_acc_z < 0.6) {
        Display_clear(hDisplay);
        Display_print0(hDisplay, 1, 1, "Bye bye");
        Display_print0(hDisplay, 2, 1, "detected!");
        strcpy(payload, "Bye!"); // viestipuskuri
        Send6LoWPAN (0xFFFF, payload, strlen(payload));
        StartReceive6LoWPAN();
        myState = msg_waiting;
    } else if (sd_acc_y > 0.8 && sd_acc_y < 1.3 && sd_gyro_x > 20 && 
    sd_gyro_x < 60 && sd_gyro_y < 60) {
        Display_clear(hDisplay);
        Display_print0(hDisplay, 1, 1, "Mars mars");
        Display_print0(hDisplay, 2, 1, "detected!");
        strcpy(payload, "Mars!"); // viestipuskuri
        Send6LoWPAN (0xFFFF, payload, strlen(payload));
        StartReceive6LoWPAN();
        myState = msg_waiting;
    } else {
        Display_clear(hDisplay);
        Display_print0(hDisplay, 3, 1, "No gesture");
        Display_print0(hDisplay, 4, 1, "detected...");
        Task_sleep(5000000 / Clock_tickPeriod);
        myState = idle;
    }
}

// **************************************************************
//
// SENSORINLUKUTASKI!!!
//
// *******************************

Void sensorTaskFxn(UArg arg0, UArg arg1) {

    // **************************************************************
    //
    // I2C INTERFACES
    //
    // *******************************
    
	I2C_Handle i2cMPU; // INTERFACE FOR MPU9250 SENSOR
	I2C_Params i2cMPUParams;

	float ax, ay, az, gx, gy, gz;

    I2C_Params_init(&i2cMPUParams);
    i2cMPUParams.bitRate = I2C_400kHz;
    i2cMPUParams.custom = (uintptr_t)&i2cMPUCfg;

    // **************************************************************
    //
    // MPU OPEN I2C
    //
    // *******************************
    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
    if (i2cMPU == NULL) {
        System_abort("Error Initializing I2CMPU\n");
    }

    // **************************************************************
    //
    // MPU POWER ON
    //
    // *******************************
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_ON);

    // WAIT 100MS FOR THE SENSOR TO POWER UP
	Task_sleep(100000 / Clock_tickPeriod);

    // **************************************************************
    //
    // MPU9250 SETUP AND CALIBRATION
    //
    // *******************************

	mpu9250_setup(&i2cMPU);

	System_printf("MPU9250: Setup and calibration OK\n");
	System_flush();

    // **************************************************************
    //
    // MPU CLOSE I2C
    //
    // *******************************
    I2C_close(i2cMPU);
    
    // LOOP FOR FIVE SECONDS
    int i = 0;
	//System_printf("starting mpu9250 sensor readout\n");
    //System_flush();
	while (1) {
	    if (myState == read_sensor) {
	        i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
            if (i2cMPU == NULL) {
                System_abort("Error Initializing I2CMPU\n");
            }
	        for (i=0;i<200;i++) {
                
                // **************************************************************
                //
                // MPU ASK DATA
                //
                //    Accelerometer values: ax,ay,az
                 //    Gyroscope values: gx,gy,gz
                //
                // *******************************
                mpu9250_get_data(&i2cMPU, &ax, &ay, &az, &gx, &gy, &gz);
                global_ax[i] = ax;
                global_ay[i] = ay;
                global_az[i] = az;
                global_gx[i] = gx;
                global_gy[i] = gy;
                global_gz[i] = gz;
                

                i++;
                Task_sleep(25000 / Clock_tickPeriod);
	        }
	        I2C_close(i2cMPU);
	        
	        detect_gestures();


	    } else {
            Task_sleep(100000 / Clock_tickPeriod);
	    }
	}


	// MPU9250 POWER OFF 
	// Because of loop forever, code never goes here
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_OFF);
}

// **************************************************************
//
// KOMMUNIKAATIO!!!
//
// *******************************

Void commTaskFxn(UArg arg0, UArg arg1) {
    
    char payload[16];
    uint8_t senderAddr;
    // Radio to receive mode
	int32_t result = StartReceive6LoWPAN();
	if(result != true) {
		System_abort("Wireless receive mode failed");
	}

    while (1) {

        // If true, we have a message
        if (GetRXFlag() && myState == msg_waiting) {
            memset(payload,0,16);
            Receive6LoWPAN(&senderAddr, payload, 16);
            Display_print0(hDisplay, 6, 1, payload);
        }

    // ABSOLUTELY NO Task_sleep in this task!!
    }
}

// **************************************************************
//
// PAINONAPIN KÄSITTELIJÄFUNKTIO
//
// *******************************

void upperButtonFxn(PIN_Handle handle, PIN_Id pinId) {
    
    if (myState == idle) {
        myState = menu_detect;
        
    } else if (myState == menu_detect) {
        myState = menu_back_to_idle;
            
    } else if (myState == menu_back_to_idle) {
        myState = menu_shutdown;
            
    } else if (myState == menu_shutdown) {
        myState = menu_detect;
    } else if (myState == msg_waiting) {
        myState = idle;
    }
}

// **************************************************************
//
// VIRTANAPIN KÄSITTELIJÄFUNKTIO
//
// *******************************

Void lowerButtonFxn(PIN_Handle handle, PIN_Id pinId) {

    if (myState == idle) {
        myState = msg_waiting;
        
    } else if (myState == menu_detect) {
        myState = read_sensor;
        
    } else if (myState == menu_back_to_idle) {
        myState = idle;
        
    } else if (myState == menu_shutdown) {
        myState = idle;
        // Näyttö pois päältä
        Display_clear(hDisplay);
        Display_close(hDisplay);
        Task_sleep(100000 / Clock_tickPeriod);
        
        // Itse taikamenot
        PIN_close(lowerButtonHandle);
        PINCC26XX_setWakeup(buttonWake);
        Power_shutdown(NULL,0);
    } else if (myState == msg_waiting) {
        myState = idle;
    }
    
}

// **************************************************************
//
// SCREEN TASK
//
// *******************************

Void screenTaskFxn(UArg arg0, UArg arg1) {
    /* Kuva: neljä 8x9 pikselin bitmappia
    11100000 00000111 11000000 11111111
    11100000 00000111 11000000 11111111
    11100000 00000111 11000000 11100000
    11100000 00000111 11000000 11000000
    11111111 11111111 11001111 11110000
    11111111 11111111 11001111 00111111
    11100000 00000111 11000000 00000011
    11100000 00000111 11000000 11000011
    11100000 00000111 11000000 11111111
    */
    int previousState;
    uint32_t imgPalette[] = {0, 0xFFFFFF};
    
    const uint8_t imgData[9] = {
        0xC0, 
        0xC0, 
        0xC0, 
        0xC0, 
        0xFF, 
        0xC0, 
        0xC0, 
        0xC0,
        0xC0
    };
    const tImage image = {
        .BPP = IMAGE_FMT_1BPP_UNCOMP,
        .NumColors = 2,
        .XSize = 1,
        .YSize = 9,
        .pPalette = imgPalette,
        .pPixel = imgData
    };
    
    const uint8_t imgData_2[9] = {
        0x03, 
        0x03, 
        0x03, 
        0x03, 
        0xFF, 
        0x03, 
        0x03, 
        0x03,
        0x03
    };
    const tImage image_2 = {
        .BPP = IMAGE_FMT_1BPP_UNCOMP,
        .NumColors = 2,
        .XSize = 1,
        .YSize = 9,
        .pPalette = imgPalette,
        .pPixel = imgData_2
    };
    
    const uint8_t imgData_3[9] = {
        0xC0, 
        0xC0, 
        0xC0, 
        0xC0, 
        0xCF, 
        0xCF, 
        0xC0, 
        0xC0,
        0xC0
    };
    const tImage image_3 = {
        .BPP = IMAGE_FMT_1BPP_UNCOMP,
        .NumColors = 2,
        .XSize = 1,
        .YSize = 9,
        .pPalette = imgPalette,
        .pPixel = imgData_3
    };
    const uint8_t imgData_4[9] = {
        0xFF, 
        0xFF, 
        0xE0, 
        0xC0, 
        0xE0, 
        0x3F, 
        0x3, 
        0xC3,
        0xFF
    };
    const tImage image_4 = {
        .BPP = IMAGE_FMT_1BPP_UNCOMP,
        .NumColors = 2,
        .XSize = 1,
        .YSize = 9,
        .pPalette = imgPalette,
        .pPixel = imgData_4
    };
    
    Display_Params displayParams;
	displayParams.lineClearMode = DISPLAY_CLEAR_BOTH;
    Display_Params_init(&displayParams);

    hDisplay = Display_open(Display_Type_LCD, &displayParams);
    if (hDisplay == NULL) {
        System_abort("Error initializing Display\n");
    }
    tContext *pContext;
	while(1) {
	    if (previousState != myState) {
            switch (myState) {
                
                case idle:
                    pContext = DisplayExt_getGrlibContext(hDisplay);
                    Display_clear(hDisplay);
                    if (pContext) {
                        GrImageDraw(pContext, &image, 27, 20);
                        GrImageDraw(pContext, &image_2, 35, 20);
                        GrImageDraw(pContext, &image_3, 48, 20);
                        GrImageDraw(pContext, &image_4, 64, 20);                        
                        GrFlush(pContext);
                    }
                    Display_print0(hDisplay, 5, 1, "Up: menu");
                    Display_print0(hDisplay, 6, 1, "Down: recei-");	
                    Display_print0(hDisplay, 7, 1, "ve messages");	
                    break;
                    
                case read_sensor:
                    Display_clear(hDisplay);
                    Display_print0(hDisplay, 1, 1, "   Detecting");
                    Display_print0(hDisplay, 2, 1, "   gestures!");	
                    break;
        
                case msg_waiting:
                    if (previousState != read_sensor) {
                        Display_clear(hDisplay);
                    }
                    Display_print0(hDisplay, 4, 1, "Receiving msgs!");	
                    break;
                    
                case menu_detect:
                    Display_clear(hDisplay);
                    Display_print0(hDisplay, 1, 1, "    Press ");
                    Display_print0(hDisplay, 2, 1, "    LOWER");
                    Display_print0(hDisplay, 3, 1, "   BUTTON");
                    Display_print0(hDisplay, 4, 1, "     to");
                    Display_print0(hDisplay, 5, 1, "    start");
                    Display_print0(hDisplay, 6, 1, "   gesture");
                    Display_print0(hDisplay, 7, 1, "  detection");
                    Display_print0(hDisplay, 8, 1, "      !");
                    break;
                    
                case menu_back_to_idle:
                    Display_clear(hDisplay);
                    Display_print0(hDisplay, 1, 1, "    Press ");
                    Display_print0(hDisplay, 2, 1, "    LOWER");
                    Display_print0(hDisplay, 3, 1, "   BUTTON");
                    Display_print0(hDisplay, 4, 1, "     to");
                    Display_print0(hDisplay, 5, 1, "     go");
                    Display_print0(hDisplay, 6, 1, "     to");
                    Display_print0(hDisplay, 7, 1, "    start");
                    Display_print0(hDisplay, 8, 1, "      !");
                    break;
                    
                case menu_shutdown:
                    Display_clear(hDisplay);
                    Display_print0(hDisplay, 1, 1, "    Press ");
                    Display_print0(hDisplay, 2, 1, "    LOWER");
                    Display_print0(hDisplay, 3, 1, "   BUTTON");
                    Display_print0(hDisplay, 4, 1, "     for");
                    Display_print0(hDisplay, 5, 1, "  shutdown");
                    Display_print0(hDisplay, 6, 1, "      !");
                
            }
	    }
        previousState = myState;
        Task_sleep(100000 / Clock_tickPeriod);

	}
}

// **************************************************************
//
// MAIN
//
// *******************************

Int main(void) {

    // **************************************************************
    //
    // TASK VARIABLES
    //
    // *******************************
    
	Task_Handle commTask;
	Task_Params commTaskParams;
	Task_Handle screenTask;
	Task_Params screenTaskParams;
	Task_Handle sensorTask;
	Task_Params sensorTaskParams;	

    // **************************************************************
    //
    // BOARD INITIALIZATION
    //
    // *******************************
    
    Board_initGeneral();
    Board_initI2C();

    // **************************************************************
    //
    // OPEN MPU POWER PIN
    //
    // *******************************
    
    hMpuPin = PIN_open(&MpuPinState, MpuPinConfig);
    if (hMpuPin == NULL) {
    	System_abort("Pin open failed!");
    }
    
    // **************************************************************
    //
    // PAINONAPPI, LEDIPINNIT JA VIRTANAPPI!!!
    //
    // *******************************
    
    upperButtonHandle = PIN_open(&upperButtonState, upperButtonConfig);
    if(!upperButtonHandle) {
       System_abort("Error initializing upper button pins\n");
    }
    
    ledHandle = PIN_open(&ledState, ledConfig);
    if(!ledHandle) {
       System_abort("Error initializing LED pins\n");
    }
    
    lowerButtonHandle = PIN_open(&lowerButtonState, lowerButtonConfig);
    if( !lowerButtonHandle ) {
      System_abort("Error initializing lower button shut pins\n");
    }

    // **************************************************************
    //
    // PAINONAPPIEN KESKEYTYSFUNKTIOT
    //
    // *******************************

    if (PIN_registerIntCb(lowerButtonHandle, &lowerButtonFxn) != 0) {
       System_abort("Error registering lower button callback function");
    }
    
    if (PIN_registerIntCb(upperButtonHandle, &upperButtonFxn) != 0) {
        System_abort("Error registering upper button callback function");
    }
   
    // **************************************************************
    //
    // TASKIT
    //
    // *******************************

    /* COMMUNICATION TASK */
    
    Init6LoWPAN(); // This function call before use!

    Task_Params_init(&commTaskParams);
    commTaskParams.stackSize = STACKSIZE;
    commTaskParams.stack = &commTaskStack;
    commTaskParams.priority=1;

    commTask = Task_create(commTaskFxn, &commTaskParams, NULL);
    if (commTask == NULL) {
    	System_abort("Task create failed!");
    }
    
    /* SCREEN TASK */
    
    Task_Params_init(&screenTaskParams);
    screenTaskParams.stackSize = STACKSIZE;
    screenTaskParams.stack = &screenTaskStack;
    screenTaskParams.priority=2;

    screenTask = Task_create(screenTaskFxn, &screenTaskParams, NULL);
    if (screenTask == NULL) {
    	System_abort("Task create failed!");
    }
    
    /* SENSOR TASK */
    
    Task_Params_init(&sensorTaskParams);
    sensorTaskParams.stackSize = STACKSIZE;
    sensorTaskParams.stack = &sensorTaskStack;
    sensorTaskParams.priority=2;

    sensorTask = Task_create(sensorTaskFxn, &sensorTaskParams, NULL);
    if (sensorTask == NULL) {
    	System_abort("Task create failed!");
    }
    
    /* KÄYNNISTYSVARMISTUS + BIOS*/
    
    System_printf("Hello world!\n");
    System_flush();
    BIOS_start();

    return (0);
}
